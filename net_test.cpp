#include "loss.hpp"
#include "sjwl"
#include "tensor.hpp"
#include <iostream>

using namespace std;
using nn::Dense;
using nn::Dmatrix;
using nn::Dtensor;
using nn::Layerlist;
using nn::Sequential;
using nn::SoftmaxCrossEntropyLoss;
using nn::Tensorlist;
using nn::Activation::ReLU;
using nn::net;
int main(int argc, char const *argv[]) {
    Dtensor xi(3);
    xi = Dtensor::Random(3);
    Dense *input = new Dense(3, 3);
    Dense *t = new Dense(3, 3);
    Dense *output = new Dense(3, 2);

    Sequential test(new net(5, input, new ReLU(), t, new ReLU(), output),new SoftmaxCrossEntropyLoss());
    cout << "train data:\n" << xi << endl;
    Dtensor yi(2);
    yi << 1, 0;
    cout << "test [0] : \n"<< "input:\t" <<xi<<"\ntarget:\t" <<yi<<"\npredict:\t" <<test.forward(xi) << endl<<endl;
    test.train(xi, yi);
    cout << "test [1] : \n"<< "input:\t" <<xi<<"\ntarget:\t" <<yi<<"\npredict:\t" <<test.forward(xi) << endl<<endl;
    test.train(xi, yi);

    return 0;
}
