#ifndef __NN_UTIL_H__
#define __NN_UTIL_H__

#include <type_traits>

namespace nn {
    template<typename T>
    typename std::enable_if<std::is_arithmetic<T>::value, T>::type
    arithmetic_constraint(T t) {
        return t;
    }

    template<typename T>
    typename std::enable_if<std::is_assignable<T, T>::value, T>::type
    assignable_constraint(T t) {
        return t;
    }

    template<typename T>
    __attribute__((unused)) int arithmetic_assert(T val) {
        static_assert(std::is_arithmetic<T>::value != 0, "Must be arithmetic type");
        return 0;
    }

}; // namespace nn
#endif