#ifndef __NN_TENSOR_H__
#define __NN_TENSOR_H__

#include "libs/Eigen/Eigen"

namespace nn {
// using Eigen::Matrix;
// using Eigen::MatrixXd;
// using Eigen::VectorXd;
    using namespace Eigen;
// template <typename Scalar>
    using Dtensor = Matrix<double, 1, -1>;
    using Darray = Eigen::ArrayXd;
    using Dmatrix = MatrixXd;
    using Tensorlist = std::vector<Dtensor *>;  // tensor list
//class Dmatrix : public MatrixXd {
//private:
//  // todo:改装使其能构造计算图和符合自动求导要求。
//public:
//  Dmatrix() : Eigen::MatrixXd() {}
//  template <typename OtherDerived>
//  explicit Dmatrix(const Eigen::MatrixBase<OtherDerived> &other)
//      : Eigen::MatrixXd(other) {}
//
//  // This method allows you to assign Eigen expressions to MyVectorType
//  template <typename OtherDerived>
//  Dmatrix &operator=(const Eigen::MatrixBase<OtherDerived> &other) {
//    this->Eigen::MatrixXd::operator=(other);
//    return *this;
//  }
//};
/*
 * If the size < 32 that we suggest to use Stensor for avoiding dynamic alloc.
 */
    template<typename Tp, int size> using Stensor __attribute__((unused)) = Matrix<Tp, size, 1>;

} // namespace nn
#endif